package com.rave.mockapp.model.remote

interface MockServer {

    /**
     * Get images from server
     * suspend - coroutine keyword that lets us move this task to another thread
     *
     * @return
     */
    suspend fun getImages(): List<String>

    companion object {

        fun getInstance(): MockServer = object : MockServer {
            override suspend fun getImages(): List<String> {
                return listOf(
                    "https://cdn.shibe.online/shibes/a2c5461e2638c0f24b14746398ec6394413aebc7.jpg",
                    "https://cdn.shibe.online/shibes/5f4e3396cce09a178b5d6c153724f0126426bd28.jpg",
                    "https://cdn.shibe.online/shibes/f9013c123132cd45517c43cbd3d061a60df44c1a.jpg",
                    "https://cdn.shibe.online/shibes/2073a373638edf4616cb61b840eb8737c420f43f.jpg",
                    "https://cdn.shibe.online/shibes/f40ea1372d3fda5e66bb2cb022f340c61afdf4d3.jpg",
                    "https://cdn.shibe.online/shibes/4ee9836abd475890a9245e3ef877d93130272ccd.jpg",
                    "https://cdn.shibe.online/shibes/ebdfd210edaf7f1c97888f8da9d83df83828907e.jpg",
                    "https://cdn.shibe.online/shibes/de9cc49365466c260c3ff8245233d89e569159f8.jpg",
                    "https://cdn.shibe.online/shibes/0f1cd42522c1d4a6cfbbc1358e5b6f65447d2381.jpg",
                    "https://cdn.shibe.online/shibes/c3f485e97b2c73fed338da48797d4057d3983595.jpg",
                    "https://cdn.shibe.online/shibes/1d4c6e4501cf0c25b24566780ebb5199ca6eb2ff.jpg",
                    "https://cdn.shibe.online/shibes/235cea79d0356d373f39ce170cb054d87cd93fd0.jpg",
                    "https://cdn.shibe.online/shibes/07411e9cb74483379a911ecea1b430307050e74a.jpg",
                    "https://cdn.shibe.online/shibes/e1ea2580cab68675734fe1dd333d00d58c671bf5.jpg",
                    "https://cdn.shibe.online/shibes/bd8073e2e408cddf00df26a37ea66c64bd2eb128.jpg",
                    "https://cdn.shibe.online/shibes/6e500da861f9349c38a411355b13b92d7ae14624.jpg",
                    "https://cdn.shibe.online/shibes/461d2dfe9418788cc58bb99d118a14db830398ae.jpg",
                    "https://cdn.shibe.online/shibes/f76a5309758241a4b82c33e9fc9a774fc8cf1f53.jpg",
                    "https://cdn.shibe.online/shibes/69991a92ec5c7083aacad90b5015b0cc89c2b087.jpg",
                    "https://cdn.shibe.online/shibes/4b7d3ca8b1f7a27c5f6a409e7dba36d1db85f08e.jpg"
                )
            }
        }
    }
}

/**
 *
[
"https://cdn.shibe.online/shibes/60c5c8b8e9b038205b13b7b00b159e6f0dcd09bb.jpg",
"https://cdn.shibe.online/shibes/a1eb74e429e7ad225d4b7c7a48bd1184b188b5d5.jpg"
]
 */