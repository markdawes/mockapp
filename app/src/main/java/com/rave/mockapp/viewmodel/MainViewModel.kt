package com.rave.mockapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.rave.mockapp.model.MockRepo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel(repo: MockRepo) : ViewModel() {

    private val _urls = MutableStateFlow(listOf<String>())
    val urls: StateFlow<List<String>> get() = _urls

    init {
        viewModelScope.launch {
            _urls.value = repo.getImages()
        }
    }

    companion object {
        fun getFactory(repo: MockRepo) = object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return MainViewModel(repo) as T
            }
        }
    }
}